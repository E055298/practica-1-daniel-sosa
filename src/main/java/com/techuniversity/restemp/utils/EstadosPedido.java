package com.techuniversity.restemp.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO,
    COMIDO;
}
