package com.techuniversity.restemp;

import com.techuniversity.restemp.controllers.EmpleadosController;
import com.techuniversity.restemp.utils.BadSeparator;
import com.techuniversity.restemp.utils.EstadosPedido;
import com.techuniversity.restemp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena() {
        String esperado = "L.U.Z D.E.L S.O.L";
        assertEquals(esperado, empleadosController.getCadena("luz del sol", "."));
    }

    @Test
    public void testBadSeparator() {
        try {
            Utilidades.getCadena("Daniel Sosa", "..");
            fail("Se esperaba Bad Separator");
        } catch (BadSeparator bs) { }
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 3, 5, -3, 15, Integer.MAX_VALUE})
    public void testEsImpar(int numero) {
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    public void testEstaBlanco(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEstaBlancoNull(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", " ", "\t", "\n"})
    public void testEstaBlancoNull1(String texto) {
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido estado) {
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }

}
