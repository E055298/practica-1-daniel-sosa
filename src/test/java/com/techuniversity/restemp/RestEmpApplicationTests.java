package com.techuniversity.restemp;

import com.techuniversity.restemp.controllers.EmpleadosController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class RestEmpApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testHome() {
        String result = empleadosController.home();
        assertEquals("Don't worry", result);
    }

}
